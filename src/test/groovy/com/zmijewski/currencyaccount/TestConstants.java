package com.zmijewski.currencyaccount;

import java.math.BigDecimal;
import java.time.LocalDate;

public class TestConstants {
    public static final LocalDate NOW = LocalDate.of(2021, 7, 21);
    public static final BigDecimal RATE = BigDecimal.valueOf(4);
}
