package com.zmijewski.currencyaccount.api

import com.zmijewski.currencyaccount.IntegrationSpecification
import com.zmijewski.currencyaccount.application.services.command.CreateAccountCommand
import com.zmijewski.currencyaccount.application.services.command.CreateSubAccountCommand
import com.zmijewski.currencyaccount.application.services.command.MakeTransferCommand
import com.zmijewski.currencyaccount.application.services.model.AccountModel
import com.zmijewski.currencyaccount.domain.account.Account
import com.zmijewski.currencyaccount.domain.account.AccountRepository
import com.zmijewski.currencyaccount.domain.subaccount.SubAccount
import com.zmijewski.currencyaccount.domain.subaccount.SubAccountCurrency
import com.zmijewski.currencyaccount.domain.subaccount.SubAccountRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.transaction.annotation.Transactional
import spock.lang.Unroll

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/sql/delete.sql")
@Transactional
class AccountApiTest extends IntegrationSpecification {
    private static final String BASE_URL = "/api/accounts"
    private static final String VALID_EXISTING_PESEL = "94101509050"
    private static final String VALID_NOT_EXISTING_PESEL = "92120508010"
    private static final String INVALID_AGE_PESEL = "10221309060"
    private static final UUID PLN_SUB_ACCOUNT_ID = UUID.randomUUID()
    private static final UUID USD_SUB_ACCOUNT_ID = UUID.randomUUID()

    @Autowired
    private AccountRepository accountRepository

    @Autowired
    private SubAccountRepository subAccountRepository

    def setup() {
        def account = prepareAccount(VALID_EXISTING_PESEL)
        prepareSubAccount(PLN_SUB_ACCOUNT_ID, SubAccountCurrency.PLN, account)
        prepareSubAccount(USD_SUB_ACCOUNT_ID, SubAccountCurrency.USD, account)
    }

    def "should not create account on same pesel"() {
        given:
        def command = new CreateAccountCommand(VALID_EXISTING_PESEL, "Jak", "Nowak", BigDecimal.ONE)

        when:
        def response = mockMvc.perform(createAccount(command)).andReturn().response

        then: "response is 400"
        response.status == HttpStatus.BAD_REQUEST.value()
    }

    def "should not create account for not 18 years old"() {
        given:
        def command = new CreateAccountCommand(INVALID_AGE_PESEL, "Jak", "Nowak", BigDecimal.ONE)

        when:
        def response = mockMvc.perform(createAccount(command)).andReturn().response

        then: "response is 400"
        response.status == HttpStatus.BAD_REQUEST.value()
    }

    @Unroll
    def "should not create account if not valid request"() {
        given:
        def command = new CreateAccountCommand(VALID_NOT_EXISTING_PESEL, firstName, lastName, amount)

        when:
        def response = mockMvc.perform(createAccount(command)).andReturn().response

        then: "response is 400"
        response.status == HttpStatus.BAD_REQUEST.value()

        where:
        firstName | lastName | amount
        null      | "Kowal"  | BigDecimal.TEN
        "Jan"     | null     | BigDecimal.TEN
        "Jan"     | "Kowal"  | null
        "Jan"     | "Kowal"  | BigDecimal.ZERO
    }

    def "should create account"() {
        given:
        def command = new CreateAccountCommand(VALID_NOT_EXISTING_PESEL, "Tomasz", "Kowal", BigDecimal.TEN)

        when:
        def response = mockMvc.perform(createAccount(command)).andReturn().response

        then: "response is 200"
        response.status == HttpStatus.OK.value()

        and: "account is created"
        def account = accountRepository.findByPesel(VALID_NOT_EXISTING_PESEL).get()
        account.pesel == VALID_NOT_EXISTING_PESEL
        account.firstName == "Tomasz"
        account.lastName == "Kowal"

        and: "subaccount is created"
        def subaccount = subAccountRepository.findAll().find { it.account == account }
        subaccount.amount == BigDecimal.TEN
        subaccount.currency == SubAccountCurrency.PLN
    }

    @Unroll
    def "should not create subaccount if not valid request"() {
        given:
        def command = new CreateSubAccountCommand(pesel, amount, currency)

        when:
        def response = mockMvc.perform(createSubAccount(command)).andReturn().response

        then: "response is 400"
        response.status == HttpStatus.BAD_REQUEST.value()

        where:
        pesel                | amount          | currency
        VALID_EXISTING_PESEL | null            | SubAccountCurrency.PLN
        VALID_EXISTING_PESEL | BigDecimal.ZERO | SubAccountCurrency.PLN
        VALID_EXISTING_PESEL | BigDecimal.TEN  | null
    }

    def "should not create subaccount if pesel not found"() {
        given:
        def command = new CreateSubAccountCommand(VALID_NOT_EXISTING_PESEL, BigDecimal.TEN, SubAccountCurrency.PLN)

        when:
        def response = mockMvc.perform(createSubAccount(command)).andReturn().response

        then: "response is 404"
        response.status == HttpStatus.NOT_FOUND.value()
    }

    def "should create subaccount"() {
        given:
        def command = new CreateSubAccountCommand(VALID_EXISTING_PESEL, BigDecimal.valueOf(5), SubAccountCurrency.USD)

        when:
        def response = mockMvc.perform(createSubAccount(command)).andReturn().response

        then: "response is 200"
        response.status == HttpStatus.OK.value()

        and: "subaccount is created"
        def subaccount = subAccountRepository.findAll().find { it.id != PLN_SUB_ACCOUNT_ID && it.id != USD_SUB_ACCOUNT_ID }
        subaccount.amount == BigDecimal.valueOf(5)
        subaccount.currency == SubAccountCurrency.USD
    }

    def "should not account if not found"() {
        when:
        def response = mockMvc.perform(getAccount(VALID_NOT_EXISTING_PESEL)).andReturn().response

        then: "response is 404"
        response.status == HttpStatus.NOT_FOUND.value()
    }

    def "should get account"() {
        when:
        def response = mockMvc.perform(getAccount(VALID_EXISTING_PESEL)).andReturn().response

        then: "response is 200"
        response.status == HttpStatus.OK.value()

        and: "data is valid"
        def account = objectMapper.readValue(response.getContentAsString(), AccountModel.class)
        account.firstName == "Jan"
        account.lastName == "Kowalski"
        account.pesel == VALID_EXISTING_PESEL
        account.subAccounts.size() == 2
        def plnSubAccount = account.subAccounts.find { it.currency == SubAccountCurrency.PLN }
        plnSubAccount.amount == BigDecimal.TEN
        plnSubAccount.id == PLN_SUB_ACCOUNT_ID
        def usdSubAccount = account.subAccounts.find { it.currency == SubAccountCurrency.USD }
        usdSubAccount.amount == BigDecimal.TEN
        usdSubAccount.id == USD_SUB_ACCOUNT_ID
    }

    @Unroll
    def "should not make transfer if not valid command"() {
        given:
        def command = new MakeTransferCommand(fromSubaccount, toSubaccount, amount)

        when:
        def response = mockMvc.perform(makeTransfer(command)).andReturn().response

        then: "response is 400"
        response.status == HttpStatus.BAD_REQUEST.value()

        where:
        fromSubaccount     | toSubaccount       | amount
        PLN_SUB_ACCOUNT_ID | PLN_SUB_ACCOUNT_ID | BigDecimal.valueOf(5)
        USD_SUB_ACCOUNT_ID | USD_SUB_ACCOUNT_ID | BigDecimal.valueOf(5)
        null               | USD_SUB_ACCOUNT_ID | BigDecimal.valueOf(5)
        USD_SUB_ACCOUNT_ID | null               | BigDecimal.valueOf(5)
        USD_SUB_ACCOUNT_ID | PLN_SUB_ACCOUNT_ID | null
        PLN_SUB_ACCOUNT_ID | USD_SUB_ACCOUNT_ID | BigDecimal.valueOf(20)
    }

    @Unroll
    def "should make transfer"() {
        given:
        def command = new MakeTransferCommand(fromSubAccount, toSubAccount, BigDecimal.valueOf(8))

        when:
        def response = mockMvc.perform(makeTransfer(command)).andReturn().response

        then: "response is 200"
        response.status == HttpStatus.OK.value()

        and: "transfer is made"
        def plnSubAccount = subAccountRepository.getById(PLN_SUB_ACCOUNT_ID)
        plnSubAccount.amount == expectedPlnAmount
        def usdSubAccount = subAccountRepository.getById(USD_SUB_ACCOUNT_ID)
        usdSubAccount.amount == expectedUsdAmount

        where:
        fromSubAccount     | toSubAccount       || expectedPlnAmount      || expectedUsdAmount
        PLN_SUB_ACCOUNT_ID | USD_SUB_ACCOUNT_ID || BigDecimal.valueOf(2)  || BigDecimal.valueOf(12)
        USD_SUB_ACCOUNT_ID | PLN_SUB_ACCOUNT_ID || BigDecimal.valueOf(42)  || BigDecimal.valueOf(2)
    }

    def prepareAccount(String pesel) {
        def account = new Account("Jan", "Kowalski", pesel)
        accountRepository.save(account)
    }

    def prepareSubAccount(UUID subAccountId, SubAccountCurrency currency, Account account) {
        def subAccount = new SubAccount(currency, BigDecimal.TEN, account)
        subAccount.id = subAccountId
        account.subAccounts.add(subAccount)
        subAccountRepository.save(subAccount)
    }

    private MockHttpServletRequestBuilder createAccount(CreateAccountCommand command) {
        post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(command))
    }

    private MockHttpServletRequestBuilder createSubAccount(CreateSubAccountCommand command) {
        post(BASE_URL + "/subaccounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(command))
    }

    private MockHttpServletRequestBuilder makeTransfer(MakeTransferCommand command) {
        post(BASE_URL + "/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(command))
    }

    private MockHttpServletRequestBuilder getAccount(String pesel = VALID_EXISTING_PESEL) {
        get(BASE_URL + "/" + pesel)
    }
}
