package com.zmijewski.currencyaccount.application.utils

import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate

class PeselUtilsTest extends Specification {

    @Unroll
    def "should get valid birthdate"() {
        when:
        def birthdate = PeselUtils.getBirthDate(pesel)

        then:
        birthdate == expected

        where:
        pesel         || expected
        "95111509050" || LocalDate.of(1995, 11, 15)
        "02211508043" || LocalDate.of(2002, 1, 15)
    }
}
