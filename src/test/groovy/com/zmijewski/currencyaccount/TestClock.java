package com.zmijewski.currencyaccount;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.Clock;
import java.time.ZoneId;

@Configuration
@Profile("test")
public class TestClock {


    @Bean
    Clock clock() {
        return Clock.fixed(TestConstants.NOW.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
    }

}
