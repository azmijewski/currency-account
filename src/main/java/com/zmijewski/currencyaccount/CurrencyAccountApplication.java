package com.zmijewski.currencyaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableFeignClients
@SpringBootApplication
public class CurrencyAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(CurrencyAccountApplication.class, args);
    }

}
