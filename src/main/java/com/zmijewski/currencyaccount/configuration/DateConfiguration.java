package com.zmijewski.currencyaccount.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.Clock;

@Configuration
@Profile("!test")
public class DateConfiguration {

    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }
}
