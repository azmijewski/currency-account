package com.zmijewski.currencyaccount.application.utils;

import java.time.LocalDate;
import java.time.Month;

public class PeselUtils {
    private static final int TWENTY_AGE = 2000;
    private static final int NINETY_AGE = 1900;

    public static LocalDate getBirthDate(String pesel) {
        var dayOfBirth = Integer.parseInt(pesel.substring(4, 6));
        var monthOfBirth = Integer.parseInt(pesel.substring(2, 4));
        var yearOfBirth = Integer.parseInt(pesel.substring(0, 2));
        if (monthOfBirth > Month.DECEMBER.getValue()) {
            monthOfBirth = monthOfBirth - 20;
            yearOfBirth = TWENTY_AGE + yearOfBirth;
        } else {
            yearOfBirth = NINETY_AGE + yearOfBirth;
        }
        return LocalDate.of(yearOfBirth, monthOfBirth, dayOfBirth);
    }
}
