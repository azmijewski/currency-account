package com.zmijewski.currencyaccount.application.transferstrategy;

import java.math.BigDecimal;

public interface AmountTransferStrategy {
    BigDecimal calculateAmount(BigDecimal amountInOtherCurrency);
}
