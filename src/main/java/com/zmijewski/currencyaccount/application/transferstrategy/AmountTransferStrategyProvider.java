package com.zmijewski.currencyaccount.application.transferstrategy;

import com.zmijewski.currencyaccount.domain.subaccount.SubAccountCurrency;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AmountTransferStrategyProvider {
    private final PlnToUsdTransferStrategy pLnToUsdTransferStrategy;
    private final UsdToPlnTransferStrategy usdToPlnTransferStrategy;

    public AmountTransferStrategy getStrategyForCurrency(SubAccountCurrency targetCurrency) {
        if (targetCurrency == SubAccountCurrency.PLN) {
            return usdToPlnTransferStrategy;
        }
        return pLnToUsdTransferStrategy;
    }
}
