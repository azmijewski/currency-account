package com.zmijewski.currencyaccount.application.transferstrategy;

import com.zmijewski.currencyaccount.application.nbpclient.NbpService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
class PlnToUsdTransferStrategy implements AmountTransferStrategy {
    private final NbpService nbpService;

    @Override
    public BigDecimal calculateAmount(BigDecimal amountInOtherCurrency) {
        var divider = nbpService.getCurrencyExchangeRate().getBid();
        return amountInOtherCurrency.divide(divider);
    }
}
