package com.zmijewski.currencyaccount.application.nbpclient.dto;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class CurrencyExchangeRate {
    String no;
    BigDecimal bid;
    BigDecimal ask;
}
