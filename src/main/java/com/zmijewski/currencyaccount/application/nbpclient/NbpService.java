package com.zmijewski.currencyaccount.application.nbpclient;

import com.zmijewski.currencyaccount.application.nbpclient.dto.CurrencyExchangeRate;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class NbpService {
    private final NbpClient nbpClient;

    public CurrencyExchangeRate getCurrencyExchangeRate() {
        try {
            return nbpClient.getCurrencyExchange().getRates()
                .stream()
                .findFirst()
                .orElseThrow(NbpApiException::new);
        } catch (FeignException ex) {
            log.error("Npb client exception", ex);
            throw new NbpApiException();
        }
    }
}
