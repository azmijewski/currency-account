package com.zmijewski.currencyaccount.application.nbpclient.dto;

import lombok.Value;

import java.util.List;

@Value
public class CurrencyExchange {
    String table;
    String currency;
    String code;
    List<CurrencyExchangeRate> rates;
}
