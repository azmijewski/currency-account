package com.zmijewski.currencyaccount.application.nbpclient;

import com.zmijewski.currencyaccount.application.nbpclient.dto.CurrencyExchange;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(
        name = "nbp",
        url = "${nbpapi.url}",
        configuration = NbpClientConfiguration.class,
        decode404 = true
)
interface NbpClient {
    @GetMapping("/exchangerates/rates/C/usd?format=json")
    CurrencyExchange getCurrencyExchange();
}
