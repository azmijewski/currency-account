package com.zmijewski.currencyaccount.application.services.command;

import com.zmijewski.currencyaccount.api.validation.CreateAccountReference;
import lombok.Value;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Value
@CreateAccountReference
public class CreateAccountCommand {
    @NotBlank
    @PESEL
    String pesel;
    @NotBlank
    String firstName;
    @NotBlank
    String lastName;
    @NotNull
    @Min(1)
    BigDecimal startAmount;
}
