package com.zmijewski.currencyaccount.application.services;

import com.zmijewski.currencyaccount.application.services.command.CreateAccountCommand;
import com.zmijewski.currencyaccount.application.services.command.CreateSubAccountCommand;
import com.zmijewski.currencyaccount.domain.account.Account;
import com.zmijewski.currencyaccount.domain.account.AccountRepository;
import com.zmijewski.currencyaccount.domain.subaccount.SubAccountCurrency;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class AccountCommandService {
    private final AccountRepository accountRepository;
    private final SubAccountCommandService subAccountCommandService;

    public UUID createAccount(CreateAccountCommand command) {
        var account = new Account(command.getFirstName(), command.getLastName(),
                command.getPesel());
        accountRepository.save(account);
        var createSubAccountCommand = new CreateSubAccountCommand(command.getPesel(),
                command.getStartAmount(), SubAccountCurrency.PLN);
        subAccountCommandService.createSubAccount(createSubAccountCommand);
        return account.getId();
    }
}
