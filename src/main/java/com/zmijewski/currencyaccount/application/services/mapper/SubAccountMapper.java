package com.zmijewski.currencyaccount.application.services.mapper;

import com.zmijewski.currencyaccount.application.services.model.SubAccountModel;
import com.zmijewski.currencyaccount.domain.subaccount.SubAccount;
import org.springframework.stereotype.Component;

@Component
public class SubAccountMapper {
    public SubAccountModel map(SubAccount subAccount) {
        return new SubAccountModel(subAccount.getId(),
                subAccount.getCurrency(), subAccount.getAmount());
    }
}
