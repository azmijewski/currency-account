package com.zmijewski.currencyaccount.application.services;

import com.zmijewski.currencyaccount.application.services.mapper.AccountMapper;
import com.zmijewski.currencyaccount.application.services.model.AccountModel;
import com.zmijewski.currencyaccount.domain.account.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class AccountQueryService {
    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    public AccountModel getByPesel(String pesel) {
        return accountRepository.findByPesel(pesel)
                .map(accountMapper::map)
                .orElseThrow(() -> new EntityNotFoundException(pesel));
    }
}
