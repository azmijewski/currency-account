package com.zmijewski.currencyaccount.application.services.model;

import lombok.Value;

import java.util.List;

@Value
public class AccountModel {
    String firstName;
    String lastName;
    String pesel;
    List<SubAccountModel> subAccounts;
}
