package com.zmijewski.currencyaccount.application.services.model;

import com.zmijewski.currencyaccount.domain.subaccount.SubAccountCurrency;
import lombok.Value;

import java.math.BigDecimal;
import java.util.UUID;

@Value
public class SubAccountModel {
    UUID id;
    SubAccountCurrency currency;
    BigDecimal amount;
}
