package com.zmijewski.currencyaccount.application.services;

import com.zmijewski.currencyaccount.application.services.command.CreateSubAccountCommand;
import com.zmijewski.currencyaccount.application.services.command.MakeTransferCommand;
import com.zmijewski.currencyaccount.application.transferstrategy.AmountTransferStrategyProvider;
import com.zmijewski.currencyaccount.domain.account.AccountRepository;
import com.zmijewski.currencyaccount.domain.subaccount.SubAccount;
import com.zmijewski.currencyaccount.domain.subaccount.SubAccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class SubAccountCommandService {
    private final SubAccountRepository subAccountRepository;
    private final AccountRepository accountRepository;
    private final AmountTransferStrategyProvider amountTransferStrategyProvider;

    public UUID createSubAccount(CreateSubAccountCommand command) {
        var account = accountRepository.findByPesel(command.getPesel())
                .orElseThrow(() -> new EntityNotFoundException(command.getPesel()));
        var subAccount = new SubAccount(command.getCurrency(), command.getAmount(), account);
        subAccountRepository.save(subAccount);
        return subAccount.getId();
    }

    public void makeTransfer(MakeTransferCommand command) {
        var sourceSubAccount = subAccountRepository.getById(command.getFromSubAccount());
        var targetSubAccount = subAccountRepository.getById(command.getToSubAccount());
        var calculateAmountStrategy = amountTransferStrategyProvider.getStrategyForCurrency(targetSubAccount.getCurrency());
        sourceSubAccount.subtractAmount(command.getAmount());
        targetSubAccount.addAmount(calculateAmountStrategy.calculateAmount(command.getAmount()));
    }
}
