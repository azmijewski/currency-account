package com.zmijewski.currencyaccount.application.services;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String pesel) {
        super(String.format("Could not find entity with pesel: %s", pesel));
    }
}
