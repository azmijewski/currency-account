package com.zmijewski.currencyaccount.application.services.command;

import com.zmijewski.currencyaccount.api.validation.TransferBetweenSubAccountsReference;
import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;

@Value
@TransferBetweenSubAccountsReference
public class MakeTransferCommand {
    @NotNull
    UUID fromSubAccount;
    @NotNull
    UUID toSubAccount;
    @NotNull
    @Min(1)
    BigDecimal amount;
}
