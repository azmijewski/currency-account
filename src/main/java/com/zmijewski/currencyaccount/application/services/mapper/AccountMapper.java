package com.zmijewski.currencyaccount.application.services.mapper;

import com.zmijewski.currencyaccount.application.services.model.AccountModel;
import com.zmijewski.currencyaccount.domain.account.Account;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class AccountMapper {
    private final SubAccountMapper subAccountMapper;

    public AccountModel map(Account account) {
        var subAccounts = account.getSubAccounts()
                .stream()
                .map(subAccountMapper::map)
                .collect(Collectors.toList());
        return new AccountModel(account.getFirstName(), account.getLastName(),
                account.getPesel(), subAccounts);
    }
}
