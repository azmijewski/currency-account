package com.zmijewski.currencyaccount.application.services.command;

import com.zmijewski.currencyaccount.domain.subaccount.SubAccountCurrency;
import lombok.Value;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Value
public class CreateSubAccountCommand {
    @NotNull
    @PESEL
    String pesel;
    @NotNull
    @Min(1)
    BigDecimal amount;
    @NotNull
    SubAccountCurrency currency;
}
