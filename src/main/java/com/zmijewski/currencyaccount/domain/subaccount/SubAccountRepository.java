package com.zmijewski.currencyaccount.domain.subaccount;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SubAccountRepository extends JpaRepository<SubAccount, UUID> {
}
