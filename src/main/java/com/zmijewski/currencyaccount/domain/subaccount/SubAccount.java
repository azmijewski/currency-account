package com.zmijewski.currencyaccount.domain.subaccount;

import com.zmijewski.currencyaccount.domain.account.Account;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Table(name = "sub_account")
public class SubAccount {
    @Id
    private UUID id;
    @Enumerated(EnumType.STRING)
    private SubAccountCurrency currency;
    private BigDecimal amount;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    public SubAccount(SubAccountCurrency currency, BigDecimal amount, Account account) {
        this.id = UUID.randomUUID();
        this.currency = currency;
        this.amount = amount;
        this.account = account;
    }

    public boolean canBeExchangedTo(SubAccount other) {
        return currency != other.currency;
    }

    public void addAmount(BigDecimal amount) {
        this.amount = this.amount.add(amount);
    }

    public void subtractAmount(BigDecimal amount) {
        this.amount = this.amount.subtract(amount);
    }
}
