package com.zmijewski.currencyaccount.domain.subaccount;

public enum SubAccountCurrency {
    PLN, USD;
}
