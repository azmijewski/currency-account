package com.zmijewski.currencyaccount.domain.account;

import com.zmijewski.currencyaccount.domain.subaccount.SubAccount;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Table(name = "account")
public class Account {
    @Id
    private UUID id;
    private String firstName;
    private String lastName;
    private String pesel;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "account")
    private List<SubAccount> subAccounts = new ArrayList<>();

    public Account(String firstName, String lastName, String pesel) {
        this.id = UUID.randomUUID();
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
    }
}
