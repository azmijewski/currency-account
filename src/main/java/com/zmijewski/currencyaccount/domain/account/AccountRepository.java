package com.zmijewski.currencyaccount.domain.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface AccountRepository extends JpaRepository<Account, UUID> {
    boolean existsByPesel(String pesel);
    Optional<Account> findByPesel(String pesel);
}
