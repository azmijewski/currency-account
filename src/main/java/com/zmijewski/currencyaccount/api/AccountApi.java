package com.zmijewski.currencyaccount.api;

import com.zmijewski.currencyaccount.application.services.AccountCommandService;
import com.zmijewski.currencyaccount.application.services.AccountQueryService;
import com.zmijewski.currencyaccount.application.services.EntityNotFoundException;
import com.zmijewski.currencyaccount.application.services.SubAccountCommandService;
import com.zmijewski.currencyaccount.application.services.command.CreateAccountCommand;
import com.zmijewski.currencyaccount.application.services.command.CreateSubAccountCommand;
import com.zmijewski.currencyaccount.application.services.command.MakeTransferCommand;
import com.zmijewski.currencyaccount.application.services.model.AccountModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/accounts")
@RequiredArgsConstructor
public class AccountApi {
    private final AccountCommandService accountCommandService;
    private final AccountQueryService accountQueryService;
    private final SubAccountCommandService subAccountCommandService;

    @GetMapping("/{pesel}")
    public AccountModel getByPesel(@PathVariable String pesel) {
        return accountQueryService.getByPesel(pesel);
    }

    @PostMapping
    public UUID createAccount(@RequestBody @Valid CreateAccountCommand command) {
        return accountCommandService.createAccount(command);
    }

    @PostMapping("/transfer")
    public void makeTransfer(@RequestBody @Valid MakeTransferCommand command) {
        subAccountCommandService.makeTransfer(command);
    }

    @PostMapping("/subaccounts")
    public UUID createSubAccount(@RequestBody @Valid CreateSubAccountCommand command) {
       return subAccountCommandService.createSubAccount(command);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void onException(EntityNotFoundException ex) {}

}
