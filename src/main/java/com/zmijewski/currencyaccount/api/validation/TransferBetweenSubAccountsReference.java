package com.zmijewski.currencyaccount.api.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = TransferBetweenSubAccountValidator.class)
@Target({TYPE})
@Retention(RUNTIME)
@Documented
public @interface TransferBetweenSubAccountsReference {
    String message() default "INVALID_TRANSFER_COMMAND";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
