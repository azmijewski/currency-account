package com.zmijewski.currencyaccount.api.validation;

import com.zmijewski.currencyaccount.application.services.command.MakeTransferCommand;
import com.zmijewski.currencyaccount.domain.subaccount.SubAccount;
import com.zmijewski.currencyaccount.domain.subaccount.SubAccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class TransferBetweenSubAccountValidator implements ConstraintValidator<TransferBetweenSubAccountsReference, MakeTransferCommand> {
    private final SubAccountRepository subAccountRepository;

    @Override
    public boolean isValid(MakeTransferCommand value, ConstraintValidatorContext context) {
        return value.getToSubAccount() == null ||
                value.getFromSubAccount() == null ||
                value.getAmount() == null ||
                areSubAccountsValid(value);
    }

    private boolean areSubAccountsValid(MakeTransferCommand command) {
        return subAccountRepository.findById(command.getFromSubAccount())
                .filter(subAccount -> isSubAccountAmountBiggerOrEquals(subAccount, command.getAmount()))
                .map(subAccount -> isOtherSubAccountValid(subAccount, command.getToSubAccount()))
                .orElse(false);
    }

    private boolean isSubAccountAmountBiggerOrEquals(SubAccount sourceSubAccount, BigDecimal amount) {
        return sourceSubAccount.getAmount().compareTo(amount) >= 0;
    }

    private boolean isOtherSubAccountValid(SubAccount sourceSubAccount, UUID targetSubAccountId) {
        return subAccountRepository.findById(targetSubAccountId)
                .filter(targetSubAccount -> areSubAccountsFromSameAccount(sourceSubAccount, targetSubAccount))
                .map(sourceSubAccount::canBeExchangedTo)
                .orElse(false);
    }

    private boolean areSubAccountsFromSameAccount(SubAccount sourceSubAccount, SubAccount targetSubAccount) {
        var sourceAccountId = sourceSubAccount.getAccount().getId();
        var targetAccountId = targetSubAccount.getAccount().getId();
        return sourceAccountId.equals(targetAccountId);
    }


}
