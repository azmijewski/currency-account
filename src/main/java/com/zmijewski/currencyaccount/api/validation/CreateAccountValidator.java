package com.zmijewski.currencyaccount.api.validation;

import com.zmijewski.currencyaccount.application.services.command.CreateAccountCommand;
import com.zmijewski.currencyaccount.application.utils.PeselUtils;
import com.zmijewski.currencyaccount.domain.account.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.Clock;
import java.time.LocalDate;
import java.time.Period;

@Component
@RequiredArgsConstructor
public class CreateAccountValidator implements ConstraintValidator<CreateAccountReference, CreateAccountCommand> {
    private final AccountRepository accountRepository;
    private final Clock clock;

    @Override
    public boolean isValid(CreateAccountCommand value, ConstraintValidatorContext context) {
        return value.getPesel() == null || isPeselValid(value.getPesel());
    }

    private boolean isPeselValid(String pesel) {
        return isAccountWithSamePeselNotExist(pesel) && hasEighteenYearsOld(pesel);
    }

    private boolean isAccountWithSamePeselNotExist(String pesel) {
        return !accountRepository.existsByPesel(pesel);
    }

    private boolean hasEighteenYearsOld(String pesel) {
        var birthDate = PeselUtils.getBirthDate(pesel);
        var now = LocalDate.now(clock);
        return Period.between(birthDate, now).getYears() >= 18;
    }
}
