create table sub_account
(
    id         uuid    not null primary key,
    currency   char(3) not null,
    amount     decimal not null,
    account_id uuid    not null
);

alter table sub_account
    add constraint sub_account__account_id_fk foreign key (account_id) references account (id);