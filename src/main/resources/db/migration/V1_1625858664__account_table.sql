create table account (
    id uuid not null primary key,
    first_name varchar(500) not null,
    last_name varchar(500),
    pesel char(11)
);

create index if not exists account__pesel_idx on account(pesel);